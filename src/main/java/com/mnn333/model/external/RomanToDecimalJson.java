package com.mnn333.model.external;

public class RomanToDecimalJson {
    private String romanNum;
    private String decimalNum;

    public String getRomanNum() {
        return romanNum;
    }

    public void setRomanNum(String romanNum) {
        this.romanNum = romanNum;
    }

    public String getDecimalNum() {
        return decimalNum;
    }

    public void setDecimalNum(String decimalNum) {
        this.decimalNum = decimalNum;
    }

}
