package com.mnn333;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RomanToDecimalApplication {
    public static void main(String[] args) {
        SpringApplication.run(RomanToDecimalApplication.class, args);
    }
}
