package com.mnn333.util;

import static com.mnn333.constants.Messages.ERROR_REQUEST_BODY_MISSING;
import static com.mnn333.constants.Constants.FORMAT_KEY_VALUE_PAIR_JSON_STR;

import com.mnn333.constants.ErrorCodes;
import com.mnn333.constants.Logs;
import com.mnn333.error.BaseRuntimeException;
import com.mnn333.model.external.ErrorJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.ServletException;
import java.util.List;

public final class ErrorUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorUtils.class);

    public ErrorUtils() {
    }

    public static HttpStatus resolveAnnotatedResponseStatus(Throwable throwable) {
        ResponseStatus annotation = AnnotationUtils.findAnnotation(throwable.getClass(), ResponseStatus.class);
        if (annotation != null)
            return annotation.value();

        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public static ErrorJson generateErrorJson(Throwable throwable) {
        HttpStatus status = null;
        String code = null;
        String message = null;

        if (throwable != null) {
            if (throwable instanceof BaseRuntimeException) {
                status = resolveAnnotatedResponseStatus(throwable);
                code = ((BaseRuntimeException) throwable).getCode();
                message = throwable.getMessage();
            } else if (throwable instanceof HttpMediaTypeException || throwable instanceof HttpMessageNotReadableException
                    || throwable instanceof MissingServletRequestParameterException) {
                status = HttpStatus.BAD_REQUEST;
                code = ErrorCodes.REQUEST_INVALID;
                message = ERROR_REQUEST_BODY_MISSING;
            } else if ( throwable instanceof MethodArgumentNotValidException) {
                status = HttpStatus.BAD_REQUEST;
                code = ErrorCodes.REQUEST_INVALID_PARAMETERS;
                message = generateErrorMessage(
                        ((MethodArgumentNotValidException) throwable).getBindingResult().getFieldErrors());
            }
        } else
            return generateErrorJson(new ServletException(throwable));

        ErrorJson errorJson = new ErrorJson();
        errorJson.setStatus(Integer.valueOf(status.value()));
        errorJson.setCode(code);
        errorJson.setMessage(message);

        LOGGER.warn(Logs.ERROR_SHOW, errorJson.getStatus(), errorJson.getCode(), errorJson.getMessage(),
                throwable.getClass().getSimpleName());

        return errorJson;
    }

    private static String generateErrorMessage(List<FieldError> fieldErrors) {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (FieldError fe : fieldErrors) {
            if (sb.length() > 1)
                sb.append(',');
            sb.append(String.format(FORMAT_KEY_VALUE_PAIR_JSON_STR, fe.getField(), fe.getDefaultMessage()));
        }
        sb.append(']');
        return sb.toString();
    }
}
