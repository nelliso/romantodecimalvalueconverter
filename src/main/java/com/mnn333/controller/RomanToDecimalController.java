package com.mnn333.controller;

import static com.mnn333.constants.Constants.PATH_ROMAN_TO_DECIMAL;

import com.mnn333.model.external.RomanToDecimalJson;
import com.mnn333.model.external.RomanValueValidationJson;
import com.mnn333.service.RomanToDecimalService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class RomanToDecimalController {

    private final RomanToDecimalService romanToDecimalService;

    public RomanToDecimalController(RomanToDecimalService romanToDecimalService) {
        this.romanToDecimalService = romanToDecimalService;
    }

    @PostMapping(path = PATH_ROMAN_TO_DECIMAL, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public RomanToDecimalJson converter(@RequestBody @Valid RomanValueValidationJson romanValueValidationJson) {

        return this.romanToDecimalService.romanToDecimalConversion(romanValueValidationJson);
    }
}
