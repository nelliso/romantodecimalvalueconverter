package com.mnn333.service;

import com.mnn333.model.external.RomanToDecimalJson;
import com.mnn333.model.external.RomanValueValidationJson;

public interface RomanToDecimalService {
    RomanToDecimalJson romanToDecimalConversion(RomanValueValidationJson romanValueValidationJson);
}
