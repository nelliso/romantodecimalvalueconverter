package com.mnn333.service;

import com.mnn333.error.NumberNotRomanNumeral;
import com.mnn333.error.RomanNumIsEmpty;
import com.mnn333.model.external.RomanToDecimalJson;
import com.mnn333.model.external.RomanValueValidationJson;
import org.springframework.stereotype.Service;

@Service
public class RomanToDecimalServiceImpl implements RomanToDecimalService{

    @Override
    public RomanToDecimalJson romanToDecimalConversion(RomanValueValidationJson romanValueValidationJson) {
        String romanNum = romanValueValidationJson.getRomanNum();

        // check if the romanNum value is empty
        if (romanNum.isEmpty() || romanNum == null) {
            throw new RomanNumIsEmpty();
        }

        // capitalize all letters
        String romanToUpperCase = romanNum.toUpperCase();

        // method that converts roman to decimal value
        int decimalNum = romanToDecimalParser(romanToUpperCase);

        // create new object for RomanToDecimalJson
        RomanToDecimalJson romanToDecimalJson = new RomanToDecimalJson();
        romanToDecimalJson.setDecimalNum(Integer.toString(decimalNum));
        romanToDecimalJson.setRomanNum(romanToUpperCase);
        return romanToDecimalJson;
    }

    private int romanToDecimalParser(String romanNumber) {

        // find length of string
        int length = romanNumber.length();

        int number = 0;
        int decimalNum = 0;
        int previousNum = 0;

        for (int i = length - 1; i >= 0; i--) {
            char romanNum = romanNumber.charAt(i);

            switch (romanNum) {
                case 'I':
                    previousNum = number;
                    number = 1;
                    break;
                case 'V':
                    previousNum = number;
                    number = 5;
                    break;
                case 'X':
                    previousNum = number;
                    number = 10;
                    break;
                case 'L':
                    previousNum = number;
                    number = 50;
                    break;
                case 'C':
                    previousNum = number;
                    number = 100;
                    break;
                case 'D':
                    previousNum = number;
                    number = 500;
                    break;
                case 'M':
                    previousNum = number;
                    number = 1000;
                    break;
                default:
                    number = -1;
                    break;
            }

            if (number == -1) { // if number is -1, then the string contained a non roman numeral
                throw new NumberNotRomanNumeral();
            } else {
                if (number < previousNum) {
                    decimalNum -= number;
                } else {
                    decimalNum += number;
                }
            }
        }

        return decimalNum;
    }
}
