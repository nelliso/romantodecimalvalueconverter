package com.mnn333.error;

import com.mnn333.constants.ErrorCodes;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static com.mnn333.constants.Messages.ERROR_NUMBER_IS_NOT_ROMAN_NUMERAL;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NumberNotRomanNumeral extends BaseRuntimeException{

    public NumberNotRomanNumeral() {
        super(ErrorCodes.USER_ENTERED_NON_ROMAN_NUMERAL, ERROR_NUMBER_IS_NOT_ROMAN_NUMERAL);
    }
}
