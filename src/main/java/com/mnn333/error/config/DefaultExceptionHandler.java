package com.mnn333.error.config;

import com.mnn333.constants.Logs;
import com.mnn333.model.external.ErrorJson;
import com.mnn333.util.ErrorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class DefaultExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler
    public ResponseEntity<ErrorJson> handler(Exception e, HttpServletRequest httpServletRequest) {
        LOGGER.trace(Logs.ERROR_HANDLER, e.getClass().getSimpleName(), e.getMessage());
        ErrorJson errorJson = ErrorUtils.generateErrorJson(e);
        BodyBuilder bodyBuilder = ResponseEntity.status(errorJson.getStatus().intValue());

        // check status is not null
        if (errorJson.getStatus() != null)
            return bodyBuilder.body(errorJson);

        return bodyBuilder.build();
    }
}
