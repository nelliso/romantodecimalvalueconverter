package com.mnn333.error;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public abstract class BaseRuntimeException extends RuntimeException {

    private final String code;

    public BaseRuntimeException(String code, String message, Object... args) {
        this(code, message, null, args);
    }

    public BaseRuntimeException(String code, String message, Throwable cause, Object... args) {
        super(args != null ? String.format(message, args) : message, cause);
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
