package com.mnn333.error;

import static com.mnn333.constants.Messages.ERROR_USER_MUST_NOT_LEAVE_FIELD_EMPTY;

import com.mnn333.constants.ErrorCodes;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RomanNumIsEmpty extends BaseRuntimeException {

    public RomanNumIsEmpty() {
        super(ErrorCodes.USER_MUST_NOT_LEAVE_FIELD_EMPTY, ERROR_USER_MUST_NOT_LEAVE_FIELD_EMPTY);
    }
}
