package com.mnn333.constants;

public final class ErrorCodes {

    public ErrorCodes() {
    }

    private static final String INVALID = "001";
    private static final String NOT_FOUND = "002";
    private static final String WRONG = "003";

    private static final String REQUEST = "R-";
    public static final String REQUEST_INVALID = REQUEST + INVALID;
    public static final String REQUEST_INVALID_PARAMETERS = REQUEST + INVALID + "-P";

    private static final String USER = "U-";
    public static final String USER_MUST_NOT_LEAVE_FIELD_EMPTY = USER + NOT_FOUND + "-NLFE" ;
    public static final String USER_ENTERED_NON_ROMAN_NUMERAL = USER + WRONG + "-NRN";
}
