package com.mnn333.constants;

public final class Logs {

    private Logs() {

    }

    public static final String ERROR_SHOW = "[{} -- {}]: {} [{}]";
    public static final String ERROR_HANDLER = "Handler error {}: {}";

}
