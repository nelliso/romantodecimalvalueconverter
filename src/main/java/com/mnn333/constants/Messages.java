package com.mnn333.constants;

public final class Messages {

    public Messages() {
    }

    public static final String ERROR_REQUEST_BODY_MISSING = "Parameter 'romanNum' is required";
    public static final String ERROR_USER_MUST_NOT_LEAVE_FIELD_EMPTY = "Roman numeral field is empty";
    public static final String ERROR_NUMBER_IS_NOT_ROMAN_NUMERAL = "Only roman numerals are required";
}
