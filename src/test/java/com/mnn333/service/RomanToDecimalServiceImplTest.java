package com.mnn333.service;

import com.mnn333.error.NumberNotRomanNumeral;
import com.mnn333.error.RomanNumIsEmpty;
import com.mnn333.model.external.RomanToDecimalJson;
import com.mnn333.model.external.RomanValueValidationJson;
import org.junit.Test;
import org.junit.runner.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RomanToDecimalServiceImplTest {

    @Mock
    RomanToDecimalService romanToDecimalService;

    @Captor
    ArgumentCaptor<RomanValueValidationJson> classArgumentCaptor;


    @Test(expected = RomanNumIsEmpty.class)
    public void testRomanToDecimalConversion_WhenRomanNumeralIsEmpty() {

        RomanValueValidationJson romanValueValidationJson = new RomanValueValidationJson();
        romanValueValidationJson.setRomanNum("");

        given(romanToDecimalService.romanToDecimalConversion(romanValueValidationJson)).willThrow(new RomanNumIsEmpty());

        romanToDecimalService.romanToDecimalConversion(romanValueValidationJson);

    }

    @Test
    public void testRomanToDecimalConversion_WhenRomanNumeralIsProvided() {
        String value = "mcmdclxxiv";
        RomanValueValidationJson romanValueValidationJson = new RomanValueValidationJson();
        romanValueValidationJson.setRomanNum(value);

        RomanToDecimalJson romanToDecimalJson = new RomanToDecimalJson();
        romanToDecimalJson.setRomanNum(romanValueValidationJson.getRomanNum());
        romanToDecimalJson.setDecimalNum("2574");

        given(romanToDecimalService.romanToDecimalConversion(romanValueValidationJson)).willReturn(romanToDecimalJson);

        RomanToDecimalJson romanToDecimal = romanToDecimalService.romanToDecimalConversion(romanValueValidationJson);

        String decimalNum = romanToDecimal.getDecimalNum();

        then(romanToDecimalService).should(times(1)).romanToDecimalConversion(classArgumentCaptor.capture());
        assertThat(classArgumentCaptor.getValue().getRomanNum(), is(value));
        assertThat("2574", is(decimalNum));

    }

    @Test(expected = NumberNotRomanNumeral.class)
    public void testRomanToDecimalConversion_NonRomanNumeral() {

        RomanValueValidationJson romanValueValidationJson = new RomanValueValidationJson();
        romanValueValidationJson.setRomanNum("mdp");

        given(romanToDecimalService.romanToDecimalConversion(romanValueValidationJson)).willThrow(new NumberNotRomanNumeral());

        romanToDecimalService.romanToDecimalConversion(romanValueValidationJson);

    }
}
