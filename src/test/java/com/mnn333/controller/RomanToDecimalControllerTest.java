package com.mnn333.controller;

import static com.mnn333.constants.Constants.PATH_ROMAN_TO_DECIMAL;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import com.google.gson.Gson;
import com.mnn333.model.external.RomanToDecimalJson;
import com.mnn333.model.external.RomanValueValidationJson;
import com.mnn333.service.RomanToDecimalService;
import org.junit.*;
import org.junit.runner.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RomanToDecimalControllerTest extends AbstractControllerTest{

    private static final Logger LOGGER = LoggerFactory.getLogger(RomanToDecimalControllerTest.class) ;

    @Mock
    private RomanToDecimalService romanToDecimalService;

    @InjectMocks
    private RomanToDecimalController romanToDecimalController;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testConverter_WhenRomanNumeralIsProvided() throws Exception{

        String value = "mcmdclxxiv";
        RomanValueValidationJson romanValueValidationJson = new RomanValueValidationJson();
        romanValueValidationJson.setRomanNum(value);

        Gson gson = new Gson();
        String json = gson.toJson(romanValueValidationJson);

        MvcResult result = this.mockMvc.perform(post(PATH_ROMAN_TO_DECIMAL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andReturn();

        int status = result.getResponse().getStatus();
        String content = result.getResponse().getContentAsString();

        // extract romanNum and decimalNum from the json
        RomanToDecimalJson romanToDecimalJson = super.mapFromJson(content, RomanToDecimalJson.class);

        assertThat("2574", is(romanToDecimalJson.getDecimalNum()));
        assertThat(200, is(status));

        LOGGER.info("\n ---- STATUS : {} -  STRING : {} ", status, romanToDecimalJson.getDecimalNum());
    }

    @Test
    public void testConverter_WhenRomanNumeralIsEmpty() throws Exception{

        String value = "";
        RomanValueValidationJson romanValueValidationJson = new RomanValueValidationJson();
        romanValueValidationJson.setRomanNum(value);

        Gson gson = new Gson();
        String json = gson.toJson(romanValueValidationJson);

        MvcResult result = this.mockMvc.perform(post(PATH_ROMAN_TO_DECIMAL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isNotFound())
                .andReturn();

        int status = result.getResponse().getStatus();
        String content = result.getResponse().getContentAsString();

        // extract error message returned
        String errorMessage = super.parseErrorMessage(content);

        assertThat("Roman numeral field is empty", is(errorMessage));
        assertThat(404, is(status));
    }

    @Test
    public void testConverter_WhenRomanNumParameterIsNotProvided() throws Exception{

        RomanValueValidationJson romanValueValidationJson = new RomanValueValidationJson();

        Gson gson = new Gson();
        String json = gson.toJson(romanValueValidationJson);

        MvcResult result = this.mockMvc.perform(post(PATH_ROMAN_TO_DECIMAL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest())
                .andReturn();

        int status = result.getResponse().getStatus();

        assertThat(400, is(status));
    }

    @Test
    public void testConverter_NonRomanNumeral() throws Exception{

        String value = "mdp";
        RomanValueValidationJson romanValueValidationJson = new RomanValueValidationJson();
        romanValueValidationJson.setRomanNum(value);

        Gson gson = new Gson();
        String json = gson.toJson(romanValueValidationJson);

        MvcResult result = this.mockMvc.perform(post(PATH_ROMAN_TO_DECIMAL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isNotFound())
                .andReturn();

        int status = result.getResponse().getStatus();
        String content = result.getResponse().getContentAsString();

        // extract error message returned
        String errorMessage = super.parseErrorMessage(content);

        assertThat("Only roman numerals are required", is(errorMessage));
        assertThat(404, is(status));
    }
}
